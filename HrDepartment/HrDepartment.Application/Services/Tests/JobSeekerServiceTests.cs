﻿using NUnit.Framework;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using HrDepartment.Infrastructure.Interfaces;
using HrDepartment.Domain.Entities;
using System.Threading.Tasks;
using HrDepartment.Application.Interfaces;
using AutoMapper;

namespace HrDepartment.Application.Services.Tests
{
    class JobSeekerServiceTests
    {
        [Test]
        public void Ctor_StorageIsEmpty_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new JobSeekerService(null, GetEmptyRateService(), GetEmptyMapper(), GetEmptyNotificationService()));
        }

        [Test]
        public void Ctor_RateServiceIsEmpty_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new JobSeekerService(GetEmptyStorage(), null, GetEmptyMapper(), GetEmptyNotificationService()));
        }

        [Test]
        public void Ctor_MapperIsEmpty_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new JobSeekerService(GetEmptyStorage(), GetEmptyRateService(), null, GetEmptyNotificationService()));
        }

        [Test]
        public void Ctor_NotificationServiceIsEmpty_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new JobSeekerService(GetEmptyStorage(), GetEmptyRateService(), GetEmptyMapper(), null));
        }

        [TestCase(0)]
        [TestCase(50)]
        [TestCase(100)]
        [TestCase(-1)]
        [TestCase(-100)]
        public void RateJobSeekerAsync_RateServiceReturnsVariableRatings_ReturnsSameRating(int rating)
        {
            IStorage storage = GetStorageThatAlwaysReturns(JobSeekerSamples.GetCommonJobSeeker());
            IRateService<JobSeeker> rateService = GetRateServiceThatAlwaysReturns(rating);
            JobSeekerService jobSeekerService = new JobSeekerService(storage, rateService, GetEmptyMapper(), GetEmptyNotificationService());

            var task = jobSeekerService.RateJobSeekerAsync(0); 
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();
            var actualRating = task.Result;
            var expectedRating = rating;

            Assert.AreEqual(expectedRating, actualRating);
        }

        [Test]
        public void RateJobSeekerAsync_CallsRateService_CalculateJobSeekerRatingAsyncIsCalled()
        {
            IStorage storage = GetStorageThatAlwaysReturns(JobSeekerSamples.GetCommonJobSeeker());
            var rateServiceMock = new Mock<IRateService<JobSeeker>>();
            rateServiceMock.Setup(rs => rs.CalculateJobSeekerRatingAsync(It.IsAny<JobSeeker>()))
                .Returns(Task.Factory.StartNew(() => 0));

            JobSeekerService jobSeekerService = new JobSeekerService(storage, rateServiceMock.Object, GetEmptyMapper(), GetEmptyNotificationService());
            var task = jobSeekerService.RateJobSeekerAsync(0);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();

            rateServiceMock.Verify(rs => rs.CalculateJobSeekerRatingAsync(It.IsAny<JobSeeker>()));
        }

        [Test]
        public void RateJobSeekerAsync_CallsRateService_CalculateJobSeekerRatingAsyncIsCalledExactlyOnce()
        {
            IStorage storage = GetStorageThatAlwaysReturns(JobSeekerSamples.GetCommonJobSeeker());
            var rateServiceMock = new Mock<IRateService<JobSeeker>>();
            rateServiceMock.Setup(rs => rs.CalculateJobSeekerRatingAsync(It.IsAny<JobSeeker>()))
                .Returns(Task.Factory.StartNew(() => 0));

            JobSeekerService jobSeekerService = new JobSeekerService(storage, rateServiceMock.Object, GetEmptyMapper(), GetEmptyNotificationService());
            var task = jobSeekerService.RateJobSeekerAsync(0);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();

            rateServiceMock.Verify(rs => rs.CalculateJobSeekerRatingAsync(It.IsAny<JobSeeker>()), Times.Once());
        }

        [Test]
        public void RateJobSeekerAsync_CallsRateService_CalculateJobSeekerRatingAsyncIsCalledExactlyWithSpecifiedJobSeeker()
        {
            var specifiedJobSeeker = JobSeekerSamples.GetCommonJobSeeker();
            IStorage storage = GetStorageThatAlwaysReturns(specifiedJobSeeker);
            var rateServiceMock = new Mock<IRateService<JobSeeker>>();
            rateServiceMock.Setup(rs => rs.CalculateJobSeekerRatingAsync(It.IsAny<JobSeeker>()))
                .Returns(Task.Factory.StartNew(() => 0));

            JobSeekerService jobSeekerService = new JobSeekerService(storage, rateServiceMock.Object, GetEmptyMapper(), GetEmptyNotificationService());
            var task = jobSeekerService.RateJobSeekerAsync(0);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();

            rateServiceMock.Verify(rs => rs.CalculateJobSeekerRatingAsync(specifiedJobSeeker));
        }

        [Test]
        public void RateJobSeekerAsync_CallsStorage_GetByIdAsyncIsCalled()
        {
            var storageMock = new Mock<IStorage>();
            storageMock.Setup(s => s.GetByIdAsync<JobSeeker, int>(It.IsAny<int>()))
                .Returns(Task.Factory.StartNew(() => JobSeekerSamples.GetCommonJobSeeker()));
            var rateService = GetRateServiceThatAlwaysReturns(0);

            JobSeekerService jobSeekerService = new JobSeekerService(storageMock.Object, rateService, GetEmptyMapper(), GetEmptyNotificationService());
            var task = jobSeekerService.RateJobSeekerAsync(0);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();

            storageMock.Verify(s => s.GetByIdAsync<JobSeeker, int>(It.IsAny<int>()));
        }

        [Test]
        public void RateJobSeekerAsync_CallsStorage_GetByIdAsyncIsCalledExactlyOnce()
        {
            var storageMock = new Mock<IStorage>();
            storageMock.Setup(s => s.GetByIdAsync<JobSeeker, int>(It.IsAny<int>()))
                .Returns(Task.Factory.StartNew(() => JobSeekerSamples.GetCommonJobSeeker()));
            var rateService = GetRateServiceThatAlwaysReturns(0);

            JobSeekerService jobSeekerService = new JobSeekerService(storageMock.Object, rateService, GetEmptyMapper(), GetEmptyNotificationService());
            var task = jobSeekerService.RateJobSeekerAsync(0);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();

            storageMock.Verify(s => s.GetByIdAsync<JobSeeker, int>(It.IsAny<int>()), Times.Once());
        }

        [Test]
        public void RateJobSeekerAsync_CallsStorage_GetByIdAsyncIsCalledExactlyWithSpecifiedId()
        {
            int specifiedId = 456;

            var storageMock = new Mock<IStorage>();
            storageMock.Setup(s => s.GetByIdAsync<JobSeeker, int>(It.IsAny<int>()))
                .Returns(Task.Factory.StartNew(() => JobSeekerSamples.GetCommonJobSeeker()));
            var rateService = GetRateServiceThatAlwaysReturns(0);

            JobSeekerService jobSeekerService = new JobSeekerService(storageMock.Object, rateService, GetEmptyMapper(), GetEmptyNotificationService());
            var task = jobSeekerService.RateJobSeekerAsync(specifiedId);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();

            storageMock.Verify(s => s.GetByIdAsync<JobSeeker, int>(specifiedId));
        }

        [Test]
        public void RateJobSeekerAsync_RatingIsEqual100_NotifyRockStarFoundAsyncIsCalled()
        {
            var storage = GetStorageThatAlwaysReturns(JobSeekerSamples.GetCommonJobSeeker());
            var rateService = GetRateServiceThatAlwaysReturns(100);
            var notificationServiceMock = new Mock<INotificationService>();
            notificationServiceMock.Setup(ns => ns.NotifyRockStarFoundAsync(It.IsAny<JobSeeker>()))
                .Returns(Task.Factory.StartNew(DoNothing));

            JobSeekerService jobSeekerService = new JobSeekerService(storage, rateService, GetEmptyMapper(), notificationServiceMock.Object);
            var task = jobSeekerService.RateJobSeekerAsync(0);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();

            notificationServiceMock.Verify(ns => ns.NotifyRockStarFoundAsync(It.IsAny<JobSeeker>()));
        }

        [Test]
        public void RateJobSeekerAsync_RatingIsEqual100_NotifyRockStarFoundAsyncIsCalledExactlyOnce()
        {
            var storage = GetStorageThatAlwaysReturns(JobSeekerSamples.GetCommonJobSeeker());
            var rateService = GetRateServiceThatAlwaysReturns(100);
            var notificationServiceMock = new Mock<INotificationService>();
            notificationServiceMock.Setup(ns => ns.NotifyRockStarFoundAsync(It.IsAny<JobSeeker>()))
                .Returns(Task.Factory.StartNew(DoNothing));

            JobSeekerService jobSeekerService = new JobSeekerService(storage, rateService, GetEmptyMapper(), notificationServiceMock.Object);
            var task = jobSeekerService.RateJobSeekerAsync(0);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();

            notificationServiceMock.Verify(ns => ns.NotifyRockStarFoundAsync(It.IsAny<JobSeeker>()), Times.Once());
        }

        public void RateJobSeekerAsync_RatingIsEqual100_NotifyRockStarFoundAsyncIsCalledExactlyWithSpecifiedJobSeeker()
        {
            var specifiedJobSeeker = JobSeekerSamples.GetCommonJobSeeker();
            var storage = GetStorageThatAlwaysReturns(specifiedJobSeeker);
            var rateService = GetRateServiceThatAlwaysReturns(100);
            var notificationServiceMock = new Mock<INotificationService>();
            notificationServiceMock.Setup(ns => ns.NotifyRockStarFoundAsync(It.IsAny<JobSeeker>()))
                .Returns(Task.Factory.StartNew(DoNothing));

            JobSeekerService jobSeekerService = new JobSeekerService(storage, rateService, GetEmptyMapper(), notificationServiceMock.Object);
            var task = jobSeekerService.RateJobSeekerAsync(0);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();

            notificationServiceMock.Verify(ns => ns.NotifyRockStarFoundAsync(specifiedJobSeeker));
        }

        [TestCase(0)]
        [TestCase(101)]
        [TestCase(99)]
        [TestCase(-30)]
        public void RateJobSeekerAsync_RatingIsNotEqual100_NotifyRockStarFoundAsyncIsNotCalledAtAll(int rating)
        {
            var specifiedJobSeeker = JobSeekerSamples.GetCommonJobSeeker();
            var storage = GetStorageThatAlwaysReturns(specifiedJobSeeker);
            var rateService = GetRateServiceThatAlwaysReturns(100);
            var notificationServiceMock = new Mock<INotificationService>();
            notificationServiceMock.Setup(ns => ns.NotifyRockStarFoundAsync(It.IsAny<JobSeeker>()))
                .Returns(Task.Factory.StartNew(DoNothing));

            JobSeekerService jobSeekerService = new JobSeekerService(storage, rateService, GetEmptyMapper(), notificationServiceMock.Object);
            var task = jobSeekerService.RateJobSeekerAsync(0);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();

            notificationServiceMock.Verify(ns => ns.NotifyRockStarFoundAsync(specifiedJobSeeker));
        }

        private IStorage GetStorageThatAlwaysReturns(JobSeeker jobSeeker)
            => Mock.Of<IStorage>(s => s.GetByIdAsync<JobSeeker, int>(It.IsAny<int>()) 
                == Task.Factory.StartNew(() => jobSeeker));

        private IRateService<JobSeeker> GetRateServiceThatAlwaysReturns(int rating)
            => Mock.Of<IRateService<JobSeeker>>(rs => rs.CalculateJobSeekerRatingAsync(It.IsAny<JobSeeker>())
                == Task.Factory.StartNew(() => rating));

        private IStorage GetEmptyStorage()
            => new Mock<IStorage>().Object;

        private IRateService<JobSeeker> GetEmptyRateService()
            => new Mock<IRateService<JobSeeker>>().Object;

        private IMapper GetEmptyMapper()
            => new Mock<IMapper>().Object;

        private INotificationService GetEmptyNotificationService()
            => new Mock<INotificationService>().Object;

        private void DoNothing()
        {
            return;
        }
    }
}
