﻿using System;
using HrDepartment.Domain.Enums;
using HrDepartment.Domain.Entities;
using System.Collections.Generic;
using System.Text;

namespace HrDepartment.Application.Services.Tests
{
    public static class JobSeekerSamples
    {
        public static JobSeeker GetWalterWhite()
            => new JobSeeker()
            {
                Id = 0,
                LastName = "White",
                FirstName = "Walter",
                MiddleName = "Hartwell",
                BirthDate = new DateTime(1958, 9, 7),
                Education = EducationLevel.University,
                Status = JobSeekerStatus.Archived,
                Experience = 1,
                BadHabits = BadHabits.None
            };

        public static JobSeeker GetJessePinkman()//-15 total
            => new JobSeeker()
            {
                Id = 1,
                LastName = "Pinkman",
                FirstName = "Jesse",
                MiddleName = "Bruce",
                BirthDate = new DateTime(1984, 9, 14),//+10
                Education = EducationLevel.School,//+5
                Status = JobSeekerStatus.New,
                Experience = 0,//+5
                BadHabits = BadHabits.Drugs | BadHabits.Smoking | BadHabits.Alcoholism//-35
            };

        public static JobSeeker GetCommonJobSeeker()//25 total
            => new JobSeeker()
            {
                FirstName = "Does",
                MiddleName = "Not",
                LastName = "Matter",
                BirthDate = DateTime.Today.AddYears(-20), //+10 rating
                Id = 2,
                Education = EducationLevel.None,//+0 rating
                Status = JobSeekerStatus.New,
                Experience = 0, //+5 rating
                BadHabits = BadHabits.None //+10 rating
            };

        public static JobSeeker GetBestJobSeeker()//95 total
            => new JobSeeker()
            {
                FirstName = "Does",
                MiddleName = "Not",
                LastName = "Matter",
                BirthDate = DateTime.Today.AddYears(-20), //+10 rating
                Id = 3,
                Education = EducationLevel.University,//+35 rating
                Status = JobSeekerStatus.New,
                Experience = 20, //+40 rating
                BadHabits = BadHabits.None //+10 rating
            };
    }
}
