﻿using System;
using System.Threading.Tasks;
using HrDepartment.Domain.Entities;
using HrDepartment.Application.Interfaces;
using NUnit.Framework;
using HrDepartment.Domain.Enums;
using Moq;

namespace HrDepartment.Application.Services.Tests
{
    [TestFixture]
    class JobSeekerRateServiceTests
    {
        [Test]
        public void Ctor_SanctionServiceIsNull_ThrowsArgNullException()
        {
            ISanctionService sanctionService = null;

            Assert.Throws<ArgumentNullException>(() => new JobSeekerRateService(sanctionService));

        }

        [Test]
        public void Ctor_SanctionServiceIsNotNull_DoesntCallIsInSanctionsListAsyncMethodAtAll()
        {
            JobSeeker jobSeeker = JobSeekerSamples.GetBestJobSeeker();
            var mock = new Mock<ISanctionService>();
            JobSeekerRateService jobSeekerRateService = new JobSeekerRateService(mock.Object);

            mock.Verify(ss => ss.IsInSanctionsListAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>()), Times.Never());
        }

        [TestCase(65, -1, 10)]
        [TestCase(64, 0, 10)]
        [TestCase(19, 0, 10)]
        [TestCase(18, 1, 10)]
        public void CalculateRating_AgeOfSeekerBetwen18And65_RatingShouldBeAdded(int ageInYears, int andDays, int expectedBirthDateRating)
        {
            DateTime birthDate = DateTime.Today.AddYears(-ageInYears).AddDays(-andDays);
            JobSeeker jobSeeker = JobSeekerSamples.GetCommonJobSeeker();
            jobSeeker.BirthDate = birthDate;
            var otherRating = 15;
            ISanctionService emptySanctionService = GetEmptySanctionService();
            JobSeekerRateService jobSeekerRateService = new JobSeekerRateService(emptySanctionService);

            var task = jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker);
            if(task.Status == TaskStatus.Created) 
                task.Start();
            task.Wait();
            var actualRating = task.Result; 

            var expectedRating = otherRating + expectedBirthDateRating;
            Assert.AreEqual(expectedRating, actualRating);
        }

        [TestCase(100, 0)]
        [TestCase(66, 0)]
        [TestCase(65, 1)]
        [TestCase(65, 0)]
        public void CalculateRating_AgeOfSeekerGreaterThanOrEqual65_RatingShouldNotBeAdded(int ageInYears, int andDays)
        {
            DateTime birthDate = DateTime.Today.AddYears(-ageInYears).AddDays(-andDays);
            JobSeeker jobSeeker = JobSeekerSamples.GetCommonJobSeeker();
            jobSeeker.BirthDate = birthDate;
            var expectedRating = 15;
            ISanctionService emptySanctionService = GetEmptySanctionService();
            JobSeekerRateService jobSeekerRateService = new JobSeekerRateService(emptySanctionService);

            var task = jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();
            var actualRating = task.Result;

            Assert.AreEqual(expectedRating, actualRating);
        }

        [TestCase(18, -1)]
        [TestCase(0, 0)]
        public void CalculateRating_AgeOfSeekerLesserThan18_RatingShouldNotBeAdded(int ageInYears, int andDays)
        {
            DateTime birthDate = DateTime.Today.AddYears(-ageInYears).AddDays(-andDays);
            JobSeeker jobSeeker = JobSeekerSamples.GetCommonJobSeeker();
            jobSeeker.BirthDate = birthDate;
            var expectedRating = 15;
            ISanctionService emptySanctionService = GetEmptySanctionService();
            JobSeekerRateService jobSeekerRateService = new JobSeekerRateService(emptySanctionService);

            var task = jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();
            var actualRating = task.Result;

            Assert.AreEqual(expectedRating, actualRating);
        }

        [Test]
        public void CalculateRating_TodayIs18BirthdayOfSeeker_RatingShouldNotBeAdded()
        {
            DateTime birthDate = DateTime.Today.AddYears(-18).AddDays(-0);
            JobSeeker jobSeeker = JobSeekerSamples.GetCommonJobSeeker();
            jobSeeker.BirthDate = birthDate;
            var expectedRating = 15;
            ISanctionService emptySanctionService = GetEmptySanctionService();
            JobSeekerRateService jobSeekerRateService = new JobSeekerRateService(emptySanctionService);

            var task = jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();
            var actualRating = task.Result;

            Assert.AreEqual(expectedRating, actualRating);
        }

        [TestCase(EducationLevel.None, 0)]
        [TestCase(EducationLevel.School, 5)]
        [TestCase(EducationLevel.College, 15)]
        [TestCase(EducationLevel.University, 35)]
        public void CalculateRating_VariedEducationLevels_returnEducationRating(EducationLevel educationLevel, int expectedEducationRating)
        {
            JobSeeker jobSeeker = JobSeekerSamples.GetCommonJobSeeker();
            jobSeeker.Education = educationLevel;
            var otherRating = 25;
            ISanctionService emptySanctionService = GetEmptySanctionService();
            JobSeekerRateService jobSeekerRateService = new JobSeekerRateService(emptySanctionService);

            var task = jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker);
            if (task.Status == TaskStatus.Created) 
                task.Start();
            task.Wait();
            var actualRating = task.Result;

            var expectedRating = otherRating + expectedEducationRating;
            Assert.AreEqual(expectedRating, actualRating);
        }

        [Test]
        public void CalculateRating_OutOfRangeEducationLevel_ThrowsArgumentOutOfRangeException()
        {
            JobSeeker jobSeeker = JobSeekerSamples.GetCommonJobSeeker();
            jobSeeker.Education = (EducationLevel)Int32.MaxValue;
            ISanctionService emptySanctionService = GetEmptySanctionService();
            JobSeekerRateService jobSeekerRateService = new JobSeekerRateService(emptySanctionService);

            Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker));
        }

        [TestCase(0, 5)]
        [TestCase(1, 10)]
        [TestCase(2, 10)]
        [TestCase(3, 15)]
        [TestCase(4, 15)]
        [TestCase(5, 25)]
        [TestCase(8, 25)]
        [TestCase(10, 40)]
        [TestCase(15, 40)]
        [TestCase(20, 40)]
        public void CalculateRating_VariedExperiences_ExpectedExperienceRating(double workExperienceInYears, int expectedExperienceRating)
        {
            JobSeeker jobSeeker = JobSeekerSamples.GetCommonJobSeeker();
            jobSeeker.Experience = workExperienceInYears;
            var otherRating = 20;
            ISanctionService emptySanctionService = GetEmptySanctionService();
            JobSeekerRateService jobSeekerRateService = new JobSeekerRateService(emptySanctionService);

            var task = jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();
            var actualRating = task.Result;

            var expectedRating = otherRating + expectedExperienceRating;
            Assert.AreEqual(expectedRating, actualRating);
        }
        
        [TestCase(1.0 - 0.001, 5)]
        [TestCase(3.0 - 0.001, 10)]
        [TestCase(5.0 - 0.001, 15)]
        [TestCase(10.0 - 0.001, 25)]
        public void CalculateRating_LittleBitLesserThanThresholdValue_RatingShouldBeLesser(double workExperienceInYears, int expectedExperienceRating)
        {
            JobSeeker jobSeeker = JobSeekerSamples.GetCommonJobSeeker();
            jobSeeker.Experience = workExperienceInYears;
            var otherRating = 20;
            ISanctionService emptySanctionService = GetEmptySanctionService();
            JobSeekerRateService jobSeekerRateService = new JobSeekerRateService(emptySanctionService);

            var task = jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();
            var actualRating = task.Result;

            var expectedRating = otherRating + expectedExperienceRating;
            Assert.AreEqual(expectedRating, actualRating);
        }

        [Test]
        public void CalculateRating_NegativeWorkExperience_ThrowsArgumentOutOfRangeException()
        {
            JobSeeker jobSeeker = JobSeekerSamples.GetCommonJobSeeker();
            jobSeeker.Experience = -1;
            ISanctionService emptySanctionService = GetEmptySanctionService();
            JobSeekerRateService jobSeekerRateService = new JobSeekerRateService(emptySanctionService);

            Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker));
        }

        [TestCase(BadHabits.None, 10)]
        [TestCase(BadHabits.Smoking, 5)]
        [TestCase(BadHabits.Alcoholism, -5)]
        [TestCase(BadHabits.Smoking | BadHabits.Alcoholism, -10)]
        [TestCase(BadHabits.Drugs, -35)]
        [TestCase(BadHabits.Smoking | BadHabits.Drugs, -40)]
        [TestCase(BadHabits.Alcoholism | BadHabits.Drugs, -50)]
        [TestCase(BadHabits.Smoking | BadHabits.Alcoholism | BadHabits.Drugs, -55)]
        public void CalculateRating_VariedBadHabbits_returnExpectedHabitsRating(BadHabits badHabits, int expectedHabitsRating)
        {
            JobSeeker jobSeeker = JobSeekerSamples.GetBestJobSeeker();
            jobSeeker.BadHabits = badHabits;
            var otherRating = 85;
            ISanctionService emptySanctionService = GetEmptySanctionService();
            JobSeekerRateService jobSeekerRateService = new JobSeekerRateService(emptySanctionService);

            var task = jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();
            var actualRating = task.Result;

            var expectedRating = otherRating + expectedHabitsRating;
            Assert.AreEqual(expectedRating, actualRating);
        }

        [Test]
        public void CalculateRating_OutOfRangeBadHabbits_ThrowsArgumentOutOfRangeException()
        {
            JobSeeker jobSeeker = JobSeekerSamples.GetBestJobSeeker();
            jobSeeker.BadHabits = (BadHabits)int.MaxValue;
            ISanctionService emptySanctionService = GetEmptySanctionService();
            JobSeekerRateService jobSeekerRateService = new JobSeekerRateService(emptySanctionService);

            Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker));
        }

        [Test]//ДОБАВИТЬ МОК
        public void CalculateRating_SeekerInBlackList_RatingEqualsZero()
        {
            JobSeeker jobSeeker = JobSeekerSamples.GetWalterWhite();
            ISanctionService sanctionServiceWithOne = GetSanctionServiceWithJobSeeker(jobSeeker);
            JobSeekerRateService jobSeekerRateService = new JobSeekerRateService(sanctionServiceWithOne);

            var task = jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();
            var actualRating = task.Result;

            var expectedRating = 0;
            Assert.AreEqual(expectedRating, actualRating);
        }

        [Test]
        public void CalculateRating_SeekerIsNotInBlackList_RatingIsNotZero()
        {
            JobSeeker jobSeeker = JobSeekerSamples.GetBestJobSeeker();
            ISanctionService sanctionServiceWithOne = GetSanctionServiceWithJobSeeker(JobSeekerSamples.GetWalterWhite());
            JobSeekerRateService jobSeekerRateService = new JobSeekerRateService(sanctionServiceWithOne);

            var task = jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();
            var actualRating = task.Result;

            var unexpectedRating = 0;
            Assert.AreNotEqual(unexpectedRating, actualRating);
        }

        [Test]
        public void CalculateRating_JobSeekerIsNotInBlackList_CallsIsInSanctionsListAsyncMethod()
        {
            JobSeeker jobSeeker = JobSeekerSamples.GetBestJobSeeker();
            var mock = new Mock<ISanctionService>();
            JobSeekerRateService jobSeekerRateService = new JobSeekerRateService(mock.Object);


            var task = jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();

            mock.Verify(ss => ss.IsInSanctionsListAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>()));
        }

        [Test]
        public void CalculateRating_JobSeekerIsNotInBlackList_CallsIsInSanctionsListAsyncMethodWithDefinedArgs()
        {
            JobSeeker jobSeeker = JobSeekerSamples.GetBestJobSeeker();
            var mock = new Mock<ISanctionService>();
            JobSeekerRateService jobSeekerRateService = new JobSeekerRateService(mock.Object);


            var task = jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();

            mock.Verify(ss => ss.IsInSanctionsListAsync(jobSeeker.LastName, jobSeeker.FirstName, jobSeeker.MiddleName, jobSeeker.BirthDate));
        }

        [Test]
        public void CalculateRating_JobSeekerIsNotInBlackList_CallsIsInSanctionsListAsyncMethodExactlyOnce()
        {
            JobSeeker jobSeeker = JobSeekerSamples.GetBestJobSeeker();
            var mock = new Mock<ISanctionService>();
            JobSeekerRateService jobSeekerRateService = new JobSeekerRateService(mock.Object);


            var task = jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();

            mock.Verify(ss => ss.IsInSanctionsListAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>()), Times.Once());
        }

        [Test]
        public void CalculateRating_JobSeekerRatingMoreThan100_RatingEquals100()
        {
            JobSeeker jobSeeker = JobSeekerSamples.GetBestJobSeeker();//95 is maximum
            throw new NotImplementedException("At this moment, there is no posibility for check it");
        }

        [Test]
        public void CalculateRating_JobSeekerRatingLesserThan0_RatingEquals0()
        {
            JobSeeker jobSeeker = JobSeekerSamples.GetJessePinkman();
            ISanctionService sanctionService = GetEmptySanctionService();
            JobSeekerRateService jobSeekerRateService = new JobSeekerRateService(sanctionService);

            var task = jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker);
            if (task.Status == TaskStatus.Created)
                task.Start();
            task.Wait();
            var actualRating = task.Result;

            var expectedRating = 0;
            Assert.AreEqual(expectedRating, actualRating);
        }

        private ISanctionService GetSanctionServiceWithJobSeeker(JobSeeker jobSeeker)
        {
            Mock<ISanctionService> sanctionServiceStub = new Mock<ISanctionService>();
            sanctionServiceStub.Setup(s => s.IsInSanctionsListAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>()))
                .Returns(Task.Factory.StartNew(() => false));

            sanctionServiceStub.Setup(s => s.IsInSanctionsListAsync(jobSeeker.LastName, jobSeeker.FirstName, jobSeeker.MiddleName, jobSeeker.BirthDate))
                .Returns(Task.Factory.StartNew(() => true));

            return sanctionServiceStub.Object;
        }

        private ISanctionService GetEmptySanctionService()
        {
            Mock<ISanctionService> sanctionServiceStub = new Mock<ISanctionService>();
            sanctionServiceStub.Setup(s => s.IsInSanctionsListAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>()))
                .Returns(Task.Factory.StartNew(() => false));

            return sanctionServiceStub.Object;
        }
    }
}
